-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

local map = vim.keymap.set

map("i", "jj", "<esc>l", { silent = true })

map("n", ";", ":")

map("n", "<C-S-h>", "<cmd>tabprevious<cr>", { desc = "Previous tab" })
map("n", "<C-S-l>", "<cmd>tabnext<cr>", { desc = "Next tab" })
